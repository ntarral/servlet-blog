package net.tarral.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author nicolas tarral
 * @version : 21/03/2016 
 *	This data class is going to describe a post , with what compose a blog post.
 * 
 *
 */
public class Post {
	
	private String 	title;
	private String 	text;	
	private String 	author;	
	private String 	category;	
	private LocalDateTime date;
	private int 	postID;	
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
	
	




	/*
	 * Default constrcutor
	 */
	public Post() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	
	/**
	 * First constructor
	 * 
	 * @param First name of the person
	 * @param last name of the person
	 * @param address of the person
	 * @param city of the person
	 * @param postal code of the person
	 * @param country of the person
	 * @param person's phone number
	 * @param person's email address
	 */
	public Post(String title, String text, String author, String category, LocalDateTime date) {
		super();
		this.title = title;
		this.text = text;
		this.author = author;
		this.category = category;
		this.date = date;
	}




	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getText() {
		return text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public String getAuthor() {
		return author;
	}


	public void setAuthor(String author) {
		this.author = author;
	}


	public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}


	public LocalDateTime getDate() {
		return date;
	}
	
	public String getStringDate() {
		return date.format(formatter);
	}	


	public void setDate(LocalDateTime date) {
		this.date = date;
	}
	
	public int getPostID() {
		return postID;
	}



	public void setPostID(int postID) {
		this.postID = postID;
	}
	
	



	
}
