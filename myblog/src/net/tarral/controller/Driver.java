package net.tarral.controller;



import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.jdbc.Connection;

import net.tarral.database.Database;
import net.tarral.database.PostDB;
import net.tarral.model.Post;
import net.tarral.util.DbUtils;



/**
 * This is the Servlet class which is going to initialize the database and implement a get method which is
 * going to display the content of the database,
 * a post method which is going to validate and alter the database with new content.
 * 
 * @author Nicolas Tarral
 * @version 22/02/2016
 *
 */
@WebServlet("/servlet")
public class Driver extends HttpServlet {

	private ArrayList<String> tables;
	private static final long serialVersionUID = 1L;
	private InputStream input;
	private ServletContext context;
	private static int COUNTER = 0;
	private boolean  avail;
	private String title;
	private String category;
	private String article;
	private String author;
	private String date;
	
	//private final String REG_PHONE = "(\\()?(\\d{3})(\\))?([\\.\\-\\/ ])?(\\d{3})([\\.\\-\\/ ])?(\\d{4})";
	//private final String REG_EMAIL = "(\\w)([\\.-]?\\w+)*@(\\w+)([\\.-]?\\w+)*(\\.\\w{2,3})";
	//private final String POSTAL_CODE = "[ABCEGHJKLMNPRSTVXY]\\d[ABCEGHJ-NPRSTV-Z][ ]?\\d[ABCEGHJ-NPRSTV-Z]\\d";
	

	private String[] messages;


	private PostDB pDB;
	
	public void init() throws ServletException {
		ServletConfig config = getServletConfig();
		System.out.println(config.getServletName());
		avail = false;
		context = config.getServletContext();

		System.out.println("Launch init method.");
		Properties dbProperties = new Properties();

		input = context.getResourceAsStream("/WEB-INF/files/dbprops.properties");
		System.out.println(context.getResourceAsStream("/WEB-INF/files/dbprops.properties"));
		try {
			dbProperties.load(input);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			System.out.println(dbProperties.getProperty("Driver"));
			Class.forName(dbProperties.getProperty("Driver"));
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			System.out.println("Connect to the database...");
			Database.init(dbProperties);
			Connection con = (Connection) DriverManager.getConnection(dbProperties.getProperty("URL"), dbProperties.getProperty("User"),dbProperties.getProperty("Password"));
			tables = DbUtils.getTables(con);
			System.out.println(tables.toString());

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		pDB = new PostDB("blog"); 

		try {
			if(!Database.tableExists("blog")) {
				
				System.out.println("creating the Table");
				pDB.create();
			} else {
				System.out.println("table exist");
				System.out.println("getting all the records to check by tittle.");
				ArrayList<Post> allPosts = pDB.read();
				for(Post current : allPosts) {
					System.out.println(current.getTitle());
					System.out.println("-------------------");
				}
			
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		
	}




	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		System.out.println("in the DoGet method");
		ArrayList<Post>  posts = new ArrayList<Post>();
		ArrayList<String>  postsByDate = new ArrayList<String>();
		String postAtt = request.getParameter("post");
		if(postAtt != null) {
			if(postAtt.equals("home")) {
				System.out.println("We selected home");
				try {
					posts = pDB.read();
					if (posts.isEmpty()) {
						request.setAttribute("size", 0);
						System.out.println("There is no post yet in the Database.");
						
					} else {
						System.out.println("There is a post  in the Database. line 155");
						postsByDate = pDB.postDate();
						for(String current : postsByDate){
							System.out.println(current);
						}
						request.setAttribute("postsByDate", postsByDate);
						request.setAttribute("postsByDateSize", postsByDate.size());
						
						posts = getPostsFromDB(request, pDB);
						request.setAttribute("allPostSize", posts.size());
						request.setAttribute("allPost", posts);

						
						response.setContentType("text/html");
						response.setHeader("Cache-Control","no-cache");
						RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/index.jsp");
						dispatcher.forward(request, response);
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				

				
			}else if (postAtt.matches("\\d+-\\d+-\\d+\\s\\d+:\\d+")) {
				String dateOfPost = request.getParameter("post");
				System.out.println("We selected this date "+dateOfPost);
				try {
					ArrayList<Post> post = pDB.readByDate(dateOfPost);
					displayList(post);
					
					postsByDate = pDB.postDate();
					
					request.setAttribute("postsByDate", postsByDate);
					request.setAttribute("postsByDateSize", postsByDate.size());
					
					
					request.setAttribute("allPostSize", post.size());
					request.setAttribute("allPost", post);
					response.setContentType("text/html");
					response.setHeader("Cache-Control","no-cache");
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/index.jsp");
					dispatcher.forward(request, response);
					
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			} else if (postAtt.equals("about")) {
				System.out.println("We selected about");
				
			}else if (postAtt.equals("new")) {
				System.out.println("We selected new");
				response.setContentType("text/html");
				response.setHeader("Cache-Control","no-cache");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/post.jsp");
				dispatcher.forward(request, response);
				
				
				
			} else {
				System.out.println("We selected nothing coherent");
				try {
					posts = pDB.read();
					if (posts.isEmpty()) {
						request.setAttribute("size", 0);
						System.out.println("There is no post yet in the Database.");
						
					} else {
						System.out.println("There is a post yet in the Database. line 202");
						
						postsByDate = pDB.postDate();
						for(String current : postsByDate){
							System.out.println(current);
						}
						request.setAttribute("postsByDate", postsByDate);
						request.setAttribute("postsByDateSize", postsByDate.size());
						
						posts = getPostsFromDB(request, pDB);
						request.setAttribute("allPostSize", posts.size());
						request.setAttribute("allPost", posts);
					


					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
				

				response.setContentType("text/html");
				response.setHeader("Cache-Control","no-cache");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/index.jsp");
				dispatcher.forward(request, response);
			}
		} else {
			System.out.println("There is no link followed line 281");
			try {
				 if (pDB.emptyTable("blog")) {
					request.setAttribute("size", 0);
					System.out.println("There is no post yet in the Database.");
					
				} else {
					System.out.println("There is a post in the Database.line 254");
					postsByDate = pDB.postDate();
					/*for(String current : postsByDate){
						System.out.println(current);
					}*/
					request.setAttribute("postsByDate", postsByDate);
					request.setAttribute("postsByDateSize", postsByDate.size());
					
					posts = getPostsFromDB(request, pDB);
					displayList(posts);
					
					System.out.println("---Setting the attributes ---");
					request.setAttribute("allPostSize", posts.size());
					request.setAttribute("allPost", posts);
					response.setContentType("text/html");
					response.setHeader("Cache-Control","no-cache");
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/index.jsp");
					dispatcher.forward(request, response);
					

				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	private ArrayList<Post> getPostsFromDB(HttpServletRequest request, PostDB pDB) throws SQLException {
		ArrayList<Post> posts = new ArrayList<Post>();
		posts = pDB.readLastTwo();
		request.setAttribute("size", posts.size());
		

		return posts;
	}
	
	@SuppressWarnings("unchecked")
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("in the DoPost method");
		
		String title = request.getParameter("Title");
		String categ = request.getParameter("Category");
		String comment = request.getParameter("Comment");
		LocalDateTime postTime = LocalDateTime.now();
		System.out.println("The date :"+postTime);
		String author = "Nicolas";
		Post myPost = new Post(title, comment,author,categ,postTime);
		
		
		try {
			System.out.println(pDB.batchInsert(myPost));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
		public void displayList(ArrayList<Post> posts) {
			System.out.println("displaying...");
			for(Post current : posts) {
				System.out.println(current.getTitle());
			}
			
		}
		
		/*Enumeration<String> paramNames = request.getParameterNames();
	    while(paramNames.hasMoreElements()) {
	    	
	      String paramName = (String)paramNames.nextElement();
	      System.out.println("the name of paream is : "+paramName);
	      String[] paramValues = request.getParameterValues(paramName);
	      String paramValue = paramValues[0];
	
	    	  if (paramValue.length() == 0) {

		        	System.out.println("no elements");
		       }else {
		    	   System.out.println(paramNames.toString()+" element "+paramValue);
		    	   
		       }


	}*/
	}	



