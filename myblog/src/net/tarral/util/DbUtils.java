package net.tarral.util;



import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DbUtils {
	
	
	public static ArrayList<String> getTables(Connection con) throws SQLException {
		ArrayList<String> tables = new ArrayList<String>();
		DatabaseMetaData dbMeta = null;
		try {
			dbMeta = con.getMetaData();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ResultSet rs = dbMeta.getTables(null, null, null, 
				new String[] {"TABLE"});
		System.out.println("here is all the tables...");
		
		while(rs.next()){

			System.out.println(rs.getString("TABLE_NAME"));
			tables.add(rs.getString("TABLE_NAME"));
		//ResultSetMetaData rsmd =  rs.getMetaData();
		//rsmd.get
		}
		
		return tables;
		
		}
	

}
