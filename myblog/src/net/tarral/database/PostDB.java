package net.tarral.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import com.mysql.jdbc.ResultSetMetaData;

import net.tarral.database.Dao;
import net.tarral.database.Database;
import net.tarral.model.Post;






public class PostDB extends Dao  {
	
	public static final String TABLE_NAME= "blog";
	
	private static final String TITLE = "title";
	private static final String TEXTE = "texte";
	private static final String CATEGORY ="category";
	private static final String DATE= "date";
	private static final String AUTHOR = "author";
	private static final String POST_ID = "postId";
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
	
	
	public PostDB(String _tableName) {
		
		super(_tableName);
		System.out.println("in the constructor");
	}
	
	
	public void create() throws SQLException {
		Connection connection = null;
		Statement stmt = null;
		System.out.println("Inthe create method");

		
		String createTable = String
		        .format("CREATE TABLE %s(%s INT NOT NULL AUTO_INCREMENT, %s VARCHAR(15), %s Text, %s VARCHAR(40), %s VARCHAR(30), %s CHAR(20), PRIMARY KEY(%s) )",
		                _tableName,POST_ID, //
		                TITLE, TEXTE, CATEGORY, DATE, AUTHOR,   POST_ID);
		//System.out.println(createTable);
		connection = Database.getConnection();
		stmt = connection.createStatement();
		stmt.execute(createTable);
		
		
	}

	
	public String batchInsert(Post person) throws SQLException {
		//DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		String formattedDateTime = person.getDate().format(formatter);
		System.out.println(formattedDateTime);
		String batchQuery = null;
		PreparedStatement stmt = null;
		Connection connection = null;

		try {
			connection = Database.getConnection();


			batchQuery = "INSERT INTO "+_tableName+"(title, texte, category, date, author) values (?, ?, ?, ?, ?)";
			//System.out.println("inserting this query : "+ batchQuery);			
			stmt = connection.prepareStatement
					(batchQuery,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

			stmt.setString(1, person.getTitle());
			stmt.setString(2, person.getText());
			stmt.setString(3, person.getCategory());
			stmt.setString(4, formattedDateTime);
			stmt.setString(5, person.getAuthor());

			connection.setAutoCommit(false);
            stmt.addBatch();
           
			stmt.executeBatch();
			connection.commit();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			stmt.close();
		}
		return batchQuery;
		
	}
	
	
	public Post getLastRecord() {

		Connection connection = null;
		Statement stmt = null;
		ResultSet queryResults = null;
		Post aPost = new Post();
		
		try {
			connection = Database.getConnection();
			String select = "SELECT * FROM " +_tableName;
			System.out.println("here is the request :"+select);
			stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			queryResults = stmt.executeQuery (select);
			System.out.println(queryResults.last());
			aPost.setPostID(queryResults.getInt(POST_ID));
			aPost.setTitle(queryResults.getString(TITLE).trim());
			aPost.setText(queryResults.getString(TEXTE).trim());
			aPost.setCategory(queryResults.getString(CATEGORY).trim());
			aPost.setDate(LocalDateTime.parse(queryResults.getString(DATE),formatter));
			aPost.setAuthor(queryResults.getString(AUTHOR).trim());

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return aPost;

	}
	
	/**
	 * This method is going to retrieve all the date of the records.
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<String> postDate() throws SQLException {
		Connection connection = null;
		Statement stmt = null;
		ResultSet queryResults = null;
		
		ArrayList<String> allRecords = new ArrayList<String>();
		String select = "SELECT * FROM "+_tableName;
		
		try {
			connection = Database.getConnection();
			stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			queryResults = stmt.executeQuery (select);
			queryResults.absolute(1);
			while (queryResults.next()) 
			{
				String postDate = queryResults.getString(DATE);


				allRecords.add(postDate);

			}

			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			stmt.close();
			//connection.close();
		}
		
		return allRecords;

	}
	
	

	public ArrayList<Post> readByDate(String date) throws SQLException {
		
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet queryResults = null;
		
		ArrayList<Post> allRecords = new ArrayList<Post>();
		String select = String.format("SELECT * FROM " +_tableName +" WHERE %s='%s'", DATE, date);
		System.out.println(select);
		
		try {
			connection = Database.getConnection();
			stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			queryResults = stmt.executeQuery (select);
			queryResults.beforeFirst();
			while (queryResults.next()) 
			{
				Post aPost = new Post();
				aPost.setPostID(queryResults.getInt(POST_ID));
				aPost.setTitle(queryResults.getString(TITLE).trim());
				aPost.setText(queryResults.getString(TEXTE).trim());
				aPost.setCategory(queryResults.getString(CATEGORY).trim());
				aPost.setDate(LocalDateTime.parse(queryResults.getString(DATE),formatter));
				aPost.setAuthor(queryResults.getString(AUTHOR).trim());

				System.out.println("Title : "+aPost.getTitle());
				allRecords.add(aPost);

			}

			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			stmt.close();
			//connection.close();
		}
		
		return allRecords;

	}
	
	
	public ArrayList<Post> readLastTwo() throws SQLException {
		Connection connection = null;
		Statement stmt = null;
		ResultSet queryResults = null;
		
		ArrayList<Post> allRecords = new ArrayList<Post>();
		
		String select = "SELECT * FROM "+_tableName;
		int count = 2;
		try {
			connection = Database.getConnection();
			stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			queryResults = stmt.executeQuery (select);
			queryResults.afterLast();
			while (queryResults.previous() && count > 0) 
			{
				System.out.println("--- getting a record from the DB ---");
				Post aPost = new Post();
				aPost.setPostID(queryResults.getInt(POST_ID));
				
				aPost.setTitle(queryResults.getString(TITLE).trim());
				System.out.println(aPost.getTitle());
				aPost.setText(queryResults.getString(TEXTE).trim());
				aPost.setCategory(queryResults.getString(CATEGORY).trim());
				aPost.setDate(LocalDateTime.parse(queryResults.getString(DATE),formatter));
				aPost.setAuthor(queryResults.getString(AUTHOR).trim());
				count--;

				allRecords.add(aPost);

			}

			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			stmt.close();
			//connection.close();
		}
		
		return allRecords;

	}
	
	
	
	public ArrayList<Post> read() throws SQLException {
		Connection connection = null;
		Statement stmt = null;
		ResultSet queryResults = null;
		
		ArrayList<Post> allRecords = new ArrayList<Post>();
		
		String select = "SELECT * FROM "+_tableName;
		
		try {
			connection = Database.getConnection();
			stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			queryResults = stmt.executeQuery (select);
			queryResults.absolute(1);
			while (queryResults.next()) 
			{
				Post aPost = new Post();
				aPost.setPostID(queryResults.getInt(POST_ID));
				aPost.setTitle(queryResults.getString(TITLE).trim());
				aPost.setText(queryResults.getString(TEXTE).trim());
				aPost.setCategory(queryResults.getString(CATEGORY).trim());
				aPost.setDate(LocalDateTime.parse(queryResults.getString(DATE),formatter));
				aPost.setAuthor(queryResults.getString(AUTHOR).trim());


				allRecords.add(aPost);

			}

			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			stmt.close();
			//connection.close();
		}
		
		return allRecords;

	}
	
	
	public String delete(String post) throws SQLException {
		String sqlString = null;
		Connection connection;
		Statement statement = null;
		try {
			connection = Database.getConnection();
			statement = connection.createStatement();

			sqlString = String.format("DELETE from %s WHERE %s='%s'", TABLE_NAME, POST_ID, post);

			statement.executeUpdate(sqlString);

		} finally {
			close(statement);
		}
		return sqlString;
		
	}
	
	public String update(Post person) throws SQLException {
		String sqlString = null;
		PreparedStatement stmt = null;
		Connection connection = null;

		try {
			connection = Database.getConnection();
			sqlString = String
			        .format("UPDATE %s set %s='%s', %s='%s', %s='%s', %s='%s', %s='%s' WHERE %s='%s'",
			        		TABLE_NAME,
			        		TITLE, person.getTitle(),
			        		TEXTE, person.getText(),
			        		CATEGORY, person.getCategory(),
			        		DATE, person.getDate(),
			        		AUTHOR, person.getAuthor(),
			        		POST_ID, person.getPostID()+"");
		//	//System.out.println("update String is :"+ sqlString);
			stmt = connection.prepareStatement
					(sqlString,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			
			connection.setAutoCommit(false);
            stmt.addBatch();
			stmt.executeBatch();
			connection.commit();

			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			stmt.close();
		}
		return sqlString;

			                
			                
		
	}

	
	public void dropTable() throws SQLException {
		drop();
	}
	
	
	
	public boolean emptyTable(String table) throws SQLException {
		String sqlString = null;
		PreparedStatement stmt = null;
		Connection connection = null;
		
		connection = Database.getConnection();
		Statement statement = null;
		
		statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				ResultSet.CONCUR_UPDATABLE);
		ResultSet resultSet = statement.executeQuery("SELECT * FROM "+table);
		int rows = 0;
		if (resultSet.last()) {
		    rows = resultSet.getRow();
		    System.out.println("Number of records "+rows);
		    // Move to beginning
		    resultSet.beforeFirst();
		}
		
		if (rows < 1) {
			return true;
		}else {
			return false;
		}
		
		
		
	}

}










