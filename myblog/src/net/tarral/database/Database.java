package net.tarral.database;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;



/**
 * @author Fred Fish, A00123456
 *
 */
public class Database {

	public static final String DB_DRIVER_KEY = "Driver";
	public static final String DB_URL_KEY = "URL";
	public static final String DB_USER_KEY = "User";
	public static final String DB_PASSWORD_KEY = "Password";
	
	

	private static final Database theInstance = new Database();
	private static Connection connection;
	private static Properties properties;


	private Database() {
	}

@Deprecated
	public static void init(Properties properties) {
		if (Database.properties == null) {

			Database.properties = properties;
		}
	}


	public static Connection getConnection() throws SQLException {
		if (connection != null) {
			return connection;
		}
		try {
			connect();
		} catch (ClassNotFoundException e) {
			throw new SQLException(e);
		}

		return connection;
	}
	

	

	 private static void connect() throws ClassNotFoundException, SQLException {
		String dbDriver = properties.getProperty(DB_DRIVER_KEY);

		Class.forName(dbDriver);
		System.out.println("Driver loaded");
		connection = DriverManager.getConnection(
				properties.getProperty(DB_URL_KEY),
				properties.getProperty(DB_USER_KEY),
				properties.getProperty(DB_PASSWORD_KEY));

	}
	


	/**
	 * Close the connections to the database
	 */
	public static void shutdown() {
		if (connection != null) {
			try {
				connection.close();
				connection = null;
			} catch (SQLException e) {

			}
		}
	}

	/**
	 * Determine if the database table exists.
	 * 
	 * @param tableName
	 * @return true is the table exists, false otherwise
	 * @throws SQLException
	 */
	public static boolean tableExists(String tableName) throws SQLException {
		DatabaseMetaData databaseMetaData = getConnection().getMetaData();
		ResultSet resultSet = null;
		String rsTableName = null;

		try {
			resultSet = databaseMetaData.getTables(connection.getCatalog(),
					"%", "%", null);
			while (resultSet.next()) {
				rsTableName = resultSet.getString("TABLE_NAME");
				if (rsTableName.equalsIgnoreCase(tableName)) {
					return true;
				}
			}
		} finally {
			resultSet.close();
		}

		return false;
	}

	/**
	 * @return the theinstance
	 */
	public static Database getTheinstance() {
		return theInstance;
	}

	public static void listTables() {

		try {

			DatabaseMetaData databaseMetaData = getConnection().getMetaData();
			ResultSet res = databaseMetaData.getTables(null, null, null,
					new String[] { "TABLE" });
			System.out.println("List of tables: ");
			while (res.next()) {
				System.out.println("   " + res.getString("TABLE_CAT") + ", "
						+ res.getString("TABLE_SCHEM") + ", "
						+ res.getString("TABLE_NAME") + ", "
						+ res.getString("TABLE_TYPE") + ", "
						+ res.getString("REMARKS"));
			}
			res.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	

}
