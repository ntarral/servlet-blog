<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title> - My Blog - </title>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<LINK REL=STYLESHEET
      HREF="style.css"
      TYPE="text/css">   

</head>
<body background="img/congruent_pentagon.png">


<ul>
  <li><a name="post" value="home" href="servlet?post=home">Home</a></li>
  
  <li><a name="post" value="new" href="servlet?post=new">New Post</a></li>

  <li><a name="post" value="about" href="servlet?post=about">About Page</a></li>
  
 
</ul>


<br>

<div id="section">
<center>

<% if( (request.getAttribute("allPost")) == null) { %>

	<h2>There is no Post yet</h2>

<% } else { %>


<c:forEach items="${requestScope.allPost}" var="post">

		<h1> ${post.title} </h1>
	<br>
	<img src="img/android-clock.png" alt="Date" style="width:25px;height:25px;"> : ${post.date}		<img src="img/ios7-folder-outline.png" alt="Category" style="width:25px;height:25px;">   ${post.category }	
	<br>
	<br>
	<br>
	${post.text}
	<br>
	<font color="Brown"><strong>Author : ${post.author}</strong></font>
  	<br>
</c:forEach>
The current time is :<br><%= new java.util.Date() %><br>
</center>
</div>
<% } %>
<div id="section2">

<h2> Previous Posts </h2>

<c:forEach items="${postsByDate}" var="festival">
		<a name="post" value="home" href="servlet?post=${festival}">${festival}</a>
        
        <br>
  
</c:forEach>
<br>

</div>


<footer>

</footer>


</body>
</html>